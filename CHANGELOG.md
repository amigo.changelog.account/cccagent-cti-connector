## [1.0.24.0] - 2019-12-04
### Changed
* Friendly name attribute for installed telephone system has been added for better readability purpose. 
* Support for multiple controllers has been added using CT Gateway.

### Added
* 0 changes

### Fixed
* 0 changes

